var mydb = require('./db.js');


var express = require('express');
var app = express();
app.get('/', function (req, res) {res.sendFile(__dirname+'/public/jep.html');});


//=====send user directly to page on /page_name ========================================
app.get('/select', function (req, res) {
    res.sendFile(__dirname+'/public/select.html');
    //res.send(loadCategories);
});
app.post('/select', function(req, res) {
   res.send(mydb.selectCategories()); 
});
app.get('/login', function (req, res) {res.sendFile(__dirname+'/public/login.html');});
//app.get('/loadCategories', function(req, res) {mydb.loadCategories(req, res);});
//app.get('/select', function (req, res) {mydb.selectCategories(req,res); });
//app.get('/question/:Category/:QNum',      function (req, res) {mydb./getQuestion(req,res); });
//app.get('/answer/:CatNum/:QNum',      function (req, res) {mydb.getAnswer(req,res); });

app.use(express.static(__dirname + '/public'));
var server = app.listen(process.env.PORT, function () {console.log("Server listening at"+ process.env.PORT);});